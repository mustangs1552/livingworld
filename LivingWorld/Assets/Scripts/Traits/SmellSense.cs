﻿using System.Collections.Generic;
using MattRGeorge.Unity.ObjectPooling;
using UnityEngine;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// This sense is the creature's ability to smell other objects.
    /// This looks for Scent objects which represents a smells.
    /// </summary>
    public class SmellSense : Sense
    {
        [Tooltip("The range that this creature can smell a scent's range.")]
        [SerializeField] private AgeRangeStat rangeStat = null;
        [Tooltip("The accuracy of this creature's ability to detect a scent.")]
        [SerializeField] private AgeRangeStat accuracyStat = null;

        #region Methods
        public override List<Transform> CheckSense()
        {
            return CheckSmell(FindEligibleObjects());
        }
        public override List<Transform> CheckSense(List<Transform> objs)
        {
            return CheckSmell(objs);
        }

        public override void SenseReset()
        {
            rangeStat.StatReset();
            accuracyStat.StatReset();
        }
        public override void SenseSetup()
        {
            AgeTrait ageTrait = MainSensesTrait.Brain.GetTrait<AgeTrait>();
            if (ageTrait != null)
            {
                rangeStat.AgeTraitObj = ageTrait;
                accuracyStat.AgeTraitObj = ageTrait;
            }
            SenseReset();
        }

        /// <summary>
        /// Calculate for the accuracy for sensing the given scent.
        /// </summary>
        /// <param name="scent">The scent to check the accuracy of.</param>
        /// <returns>A value between 0 and 1 representing the accuracy.</returns>
        private float CalcAccuracy(Scent scent)
        {
            if (scent == null) return 0;

            rangeStat.UpdateAgeValue();
            accuracyStat.UpdateAgeValue();

            float dist = Vector3.Distance(transform.position, scent.transform.position);
            float maxDist = rangeStat.CurrAmount + scent.CurrRange;
            if (dist >= maxDist) return 0;
            else return (((maxDist - dist) / maxDist) + accuracyStat.CurrAmountPerc) / 2;
        }
        /// <summary>
        /// Check the given list of scents to see if this creature can smell any of them.
        /// </summary>
        /// <param name="scents">The scents to check.</param>
        /// <returns>The scents' transfoms that this creature smells.</returns>
        private List<Transform> CheckSmell(List<Transform> scents)
        {
            if (scents == null || scents.Count == 0) return new List<Transform>();

            List<Transform> sensedScents = new List<Transform>();
            foreach(Transform scentTrans in scents)
            {
                Scent scent = scentTrans.GetComponent<Scent>();
                if (scent != null)
                {
                    float accuracy = CalcAccuracy(scent);
                    if (accuracy <= 0) continue;
                    else if (accuracy >= 1) sensedScents.Add(scentTrans);
                    else
                    {
                        int randNum = Random.Range(0, 100);
                        if (accuracy >= randNum / 100) sensedScents.Add(scentTrans);
                    }
                }
            }

            return sensedScents;
        }

        protected override List<Transform> FindEligibleObjects()
        {
            List<Scent> scents = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<Scent>();
            List<Transform> scentTrans = new List<Transform>();
            scents.ForEach(x => scentTrans.Add(x.transform));
            return scentTrans;
        }
        #endregion

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();

            if (isDebug)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, rangeStat.CurrAmount);
            }
        }
    }
}
