﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.AI.AIIndividual;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// The main script that accesses the various senses of this creature.
    /// </summary>
    public class SensesTrait : PassiveTrait
    {
        #region Variables
        private Sense[] senses = null;
        #endregion

        #region Methods
        public override void PreTraitSetup()
        {
            FindSenses();
        }
        public override void TraitSetup()
        {

        }
        public override void PostTraitSetup()
        {

        }

        public override void UpdateTrait()
        {

        }

        public override void TraitReset()
        {

        }

        /// <summary>
        /// Gets all the objects currently sensed by all senses using the object pools.
        /// </summary>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public List<Transform> CheckSenses()
        {
            List<Transform> objs = new List<Transform>();

            List<Transform> currObjs = new List<Transform>();
            foreach (Sense sense in senses)
            {
                currObjs = CheckSense(sense.GetType());
                foreach (Transform obj in currObjs) objs.Add(obj);
            }

            return objs;
        }
        /// <summary>
        /// Gets all the objects in the given list currently sensed by all senses.
        /// </summary>
        /// <param name="objs">The list of objects to check.</param>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public List<Transform> CheckSenses(List<Transform> objs)
        {
            List<Transform> sensedObjs = new List<Transform>();
            foreach (Sense sense in senses)
            {
                sense.CheckSense(objs).ForEach(x => sensedObjs.Add(x));
            }

            return sensedObjs;
        }

        /// <summary>
        /// Gets all the objects currently sensed by the desired sense using the object pools.
        /// </summary>
        /// <param name="type">The typeof() sense to check.</param>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public List<Transform> CheckSense(Type type)
        {
            List<Transform> objs = new List<Transform>();

            foreach (Sense sense in senses)
            {
                if (type == sense.GetType()) objs = sense.CheckSense();
            }

            return objs;
        }
        /// <summary>
        /// Gets all the objects in the given list currently sensed by the desired sense.
        /// </summary>
        /// <param name="type">The typeof() sense to check.</param>
        /// <param name="objs">The list of objects to check.</param>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public List<Transform> CheckSense(Type type, List<Transform> objs)
        {
            List<Transform> sensedObjs = new List<Transform>();
            foreach (Sense sense in senses)
            {
                if (type == sense.GetType()) sense.CheckSense(objs).ForEach(x => sensedObjs.Add(x));
            }

            return sensedObjs;
        }

        /// <summary>
        /// Reset the SensesTrait and Senses.
        /// </summary>
        public void ResetSenses()
        {
            foreach (Sense sense in senses) sense.SenseReset();
        }

        /// <summary>
        /// Find all Sense objects on this gameobject and tell them to setup.
        /// </summary>
        private void FindSenses()
        {
            senses = GetComponents<Sense>();
            if (senses.Length > 0)
            {
                foreach (Sense sense in senses)
                {
                    sense.Setup(this);
                    sense.SenseSetup();
                }
            }
            else Debug.LogError("No senses found!");
        }
        #endregion
    }
}