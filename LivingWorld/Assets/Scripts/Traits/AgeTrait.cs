﻿using System;
using UnityEngine;
using MattRGeorge.Unity.AI.AIIndividual;
using MattRGeorge.Unity.DayNightCycle;
using MattRGeorge.Unity.DayNightCycle.Engines;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// A creature's age.
    /// This class is used to calculate the creature's age multiplier which represents where the creature is during its life.
    /// The age multiplier is used to calculate age affected stats.
    /// </summary>
    public class AgeTrait : PassiveTrait
    {
        [SerializeField] private int lifespanDays = 365;
        [Range(.1f, .9f)] [SerializeField] private float primePoint = .5f;

        /// <summary>
        /// The date of birth.
        /// </summary>
        public DNCDatestamp BirthDate
        {
            get
            {
                return birthDate;
            }
        }
        /// <summary>
        /// The lifespan in days.
        /// </summary>
        public int LifespanDays
        {
            get
            {
                return lifespanDays;
            }
        }
        /// <summary>
        /// The percentange of the lifespan where the prime is.
        /// </summary>
        public float PrimePoint
        {
            get
            {
                return primePoint;
            }
        }

        /// <summary>
        /// The current age.
        /// </summary>
        public DNCAge Age
        {
            get
            {
                return DayNightCycle.SINGLETON.DateEngineObj.FindAge(birthDate);
            }
        }

        /// <summary>
        /// The age multiplier. Goes from 0-1 while growing and 1-0 while degrading after prime.
        /// </summary>
        public float AgeMultiplier
        {
            get
            {
                return CalcAgeMultiplier();
            }
        }
        /// <summary>
        /// Are we growing or degrading.
        /// </summary>
        public bool IsGrowing
        {
            get
            {
                if (birthDate != null)
                {
                    float primePointDays = lifespanDays * primePoint;
                    DNCDatestamp now = DayNightCycle.SINGLETON.DateEngineObj.NowDatestamp;
                    ulong currDaysLived = now.totalDays - birthDate.totalDays;

                    return currDaysLived <= primePointDays;
                }

                return false;
            }
        }

        private DNCDatestamp birthDate = null;

        public override void UpdateTrait()
        {
            //Debug.Log(string.Format("Birthdate: {0}\nAge: {1}\nAge Multiplier: {2}\nIsGrowing: {3}", BirthDate.ToString(), Age.ToString(), AgeMultiplier, IsGrowing));
        }

        public override void PreTraitSetup()
        {

        }
        public override void TraitSetup()
        {

        }
        public override void PostTraitSetup()
        {
            
        }

        public override void TraitReset()
        {
            Born();
        }

        /// <summary>
        /// Born now. Saves current datestamp as birth.
        /// Will keep trying if DayNightCycle singleton isn't found.
        /// </summary>
        public void Born()
        {
            if (DayNightCycle.SINGLETON != null) birthDate = DayNightCycle.SINGLETON.DateEngineObj.NowDatestamp;
            else Invoke("Born", .1f);
        }

        /// <summary>
        /// Born late. The birth datestamp will be calculated daysLate in the past.
        /// NOTE: DayNightCycle must also have world start be in the past as it cannot go past that.
        /// </summary>
        /// <param name="daysLate">Days the birth datestamp is in the past.</param>
        public void LateBorn(int daysLate)
        {
            birthDate = DayNightCycle.SINGLETON.DateEngineObj.FindDate(-daysLate);
        }

        /// <summary>
        /// Calculates the point of age up to the prime (0-1) and then degrading after (1-0).
        /// This value can be used as a multiplier to other stats to simulate those stats growing/degrading with age.
        /// </summary>
        /// <returns>The age multiplier in the range of 0-1. -1 if cannot calculate age multiplier.</returns>
        private float CalcAgeMultiplier()
        {
            if (birthDate != null)
            {
                float primePointdays = lifespanDays * primePoint;
                DNCDatestamp now = DayNightCycle.SINGLETON.DateEngineObj.NowDatestamp;
                ulong currDaysLived = now.totalDays - birthDate.totalDays;

                float ageMultiplier = 0;
                if (currDaysLived <= (ulong)lifespanDays)
                {
                    if (currDaysLived <= primePointdays) ageMultiplier = currDaysLived / primePointdays;
                    else
                    {
                        float secsLivedPastPrime = currDaysLived - primePointdays;
                        float degradingSecs = lifespanDays - primePointdays;
                        ageMultiplier = -(secsLivedPastPrime / degradingSecs - 1);
                    }
                }

                return ageMultiplier;
            }
            else return -1;
        }

        /// <summary>
        /// Spawn with a random age between world start and now.
        /// </summary>
        private void OnSpawn()
        {
            ulong worldDaysOld = DayNightCycle.SINGLETON.DateEngineObj.NowDatestamp.totalDays;
            
            int daysBack = 0;
            try
            {
                daysBack = (int)UnityEngine.Random.Range(0, worldDaysOld);
            }
            catch(InvalidCastException e)
            {
                Debug.LogError("Unable to cast from ulong to int for datestamp total days to days back! Exception: " + e.Message);
                return;
            }
            LateBorn(daysBack);
        }
    }
}
