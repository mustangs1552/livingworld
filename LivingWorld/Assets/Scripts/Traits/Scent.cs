﻿using UnityEngine;
using MattRGeorge.Unity.ObjectPooling;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// This represents a smell.
    /// The Smell Trait looks for these objects.
    /// </summary>
    public class Scent : MonoBehaviour
    {
        #region Variables
        [Tooltip("Is debugging enabled?")]
        [SerializeField] private bool isDebug = false;
        [Tooltip("The default range value.")]
        [SerializeField] private float defaultRange = 10;

        /// <summary>
        /// The current range value.
        /// </summary>
        public float CurrRange
        {
            get
            {
                return currRange;
            }
            set
            {
                currRange = (value > 0) ? value : 0;
            }
        }

        private float currRange = 0;
        #endregion

        #region Methods
        /// <summary>
        /// Start a despawn timer that automatically despawns this object.
        /// </summary>
        /// <param name="time">The time before this object is despawned.</param>
        public void StartDespawnTimer(float time)
        {
            if(time > 0) Invoke("Despawn", time);
        }

        /// <summary>
        /// Despawn this object.
        /// </summary>
        private void Despawn()
        {
            ObjectPoolManager.SINGLETON.Destroy(gameObject);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            CurrRange = defaultRange;
        }
        #endregion

        private void OnEnable()
        {
            Setup();
        }

        private void OnDrawGizmos()
        {
            if (isDebug)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, CurrRange);
            }
        }
    }
}
