﻿using UnityEngine;
using MattRGeorge.Unity.AI.AIIndividual;
using MattRGeorge.Unity.ObjectPooling;

namespace Assets.Scripts.Traits
{
    [RequireComponent(typeof(AIBrain))]
    [RequireComponent(typeof(AgeTrait))]
    /// <summary>
    /// This trait allows a creature to give birth with out the need of another.
    /// </summary>
    public class AsexualReproductionTrait : AITrait
    {
        #region Variables
        [Tooltip("The young that this creature will spawn.")]
        [SerializeField] protected GameObject youngPrefab = null;
        [Tooltip("The max amount of younf that this creature could spawn.")]
        [SerializeField] private int maxYoungCount = 1;
        [Tooltip("The minimum age in days required to be able to reproduce.")]
        [SerializeField] private ulong minAgeDaysToReproduce = 180;
        [Tooltip("The cooldown in days after giving birth before being able to give birth again.")]
        [SerializeField] private ulong postReproductionDays = 30;
        [Tooltip("The duration in seconds that this creature will spend giving birth.")]
        [SerializeField] private float givingBirthDurationSecs = 3;

        /// <summary>
        /// Is this creature able to give birth?
        /// </summary>
        public bool CanGiveBirth
        {
            get
            {
                return ageTrait.Age.totalDays >= minAgeDaysToReproduce && ageTrait.Age.totalDays - lastBirthDays >= postReproductionDays;
            }
        }
        /// <summary>
        /// Is this creature currently giving birth.
        /// </summary>
        public bool GivingBirth
        {
            get
            {
                return birthStarted >= 0;
            }
        }

        protected AgeTrait ageTrait = null;
        protected ulong lastBirthDays = 0;
        private float birthStarted = -1;
        #endregion

        #region Methods
        public override bool Checking()
        {
            return youngPrefab != null && (CanGiveBirth || GivingBirth);
        }
        public override bool Performing()
        {
            if (birthStarted < 0) birthStarted = Time.time;
            else if (Time.time - birthStarted >= givingBirthDurationSecs)
            {
                GiveBirth();
                birthStarted = -1;
                return false;
            }

            return true;
        }

        public override void PreTraitSetup()
        {

        }
        public override void TraitSetup()
        {
            ageTrait = Brain.GetTrait<AgeTrait>();
        }
        public override void PostTraitSetup()
        {

        }

        public override bool CancelPerform()
        {
            if (GivingBirth) return false;

            return true;
        }
        public override void TraitReset()
        {
            lastBirthDays = 0;
            birthStarted = -1;
        }

        /// <summary>
        /// Give birth by spawning a random amount of young from 1 to max.
        /// </summary>
        private void GiveBirth()
        {
            if (youngPrefab == null || maxYoungCount <= 0) return;

            int randNum = Random.Range(1, maxYoungCount);
            for (int i = 0; i < randNum; i++)
            {
                AIBrain young = ObjectPoolManager.SINGLETON.Instantiate(youngPrefab, transform.position, transform.rotation).GetComponent<AIBrain>();
                if (young != null)
                {
                    young.RunSetup();
                    AgeTrait youngAgeTrait = young.GetTrait<AgeTrait>();
                    if (youngAgeTrait != null) youngAgeTrait.Born();
                }
            }

            lastBirthDays = ageTrait.Age.totalDays;
        }
        #endregion
    }
}
