﻿using System;
using UnityEngine;
using MattRGeorge.Unity.Misc;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// An extension of a dynamic stat, this stat adds support for being affected by the creature's age.
    /// </summary>
    [Serializable]
    public class DynamicAgeStat : DynamicStatGauge
    {
        #region Variables
        [Tooltip("The max amount at the creature's prime.")]
        [SerializeField] private float primeMaxAmount = 200;
        [Tooltip("Update the current amount when growing towards prime?")]
        [SerializeField] private bool updateWhenGrowing = true;
        [Tooltip("Default to max or min when 'updateWhenGrowing' is false?")]
        [SerializeField] private bool defaultToMaxWhenGrowing = true;
        [Tooltip("Update the current amount when growing past prime?")]
        [SerializeField] private bool updateWhenDegrading = true;
        [Tooltip("Default to max or min when 'updateWhenDegrading' is false?")]
        [SerializeField] private bool defaultToMaxWhenDegrading = true;

        /// <summary>
        /// The AgeTrait this stat should use to update its max with the current age.
        /// </summary>
        public AgeTrait AgeTraitObj { get; set; }
        #endregion

        public DynamicAgeStat(float sa, float sma, bool uwma, float sua, float primeMaxAmount, bool updateWhenGrowing, bool defaultToMaxWhenGrowing, bool updateWhenDegrading, bool defaultToMaxWhenDegrading, AgeTrait ageTrait) : base(sa, sma, uwma, sua)
        {
            this.primeMaxAmount = primeMaxAmount;
            this.updateWhenGrowing = updateWhenGrowing;
            this.defaultToMaxWhenGrowing = defaultToMaxWhenGrowing;
            this.updateWhenDegrading = updateWhenDegrading;
            this.defaultToMaxWhenDegrading = defaultToMaxWhenDegrading;
            AgeTraitObj = ageTrait;
        }

        /// <summary>
        /// Update the stat via the update amount and the max value with the current age.
        /// </summary>
        public override void StatUpdate()
        {
            base.StatUpdate();
            UpdateAgeValue();
        }
        /// <summary>
        /// Update this stats max value with the current age.
        /// </summary>
        public void UpdateAgeValue()
        {
            if (AgeTraitObj == null || primeMaxAmount == startingMaxAmount) return;
            else if (primeMaxAmount < startingMaxAmount)
            {
                Debug.LogError("'absoluteMaxAmount' must be greater than or equal to 'startingMaxAmount'!");
                return;
            }

            if (AgeTraitObj.IsGrowing)
            {
                if (updateWhenGrowing) CurrMaxAmount = CurrMaxAmount + ((primeMaxAmount - CurrMaxAmount) * AgeTraitObj.AgeMultiplier);
                else CurrMaxAmount = (defaultToMaxWhenGrowing) ? primeMaxAmount : startingMaxAmount;
            }
            else
            {
                if (updateWhenDegrading) CurrMaxAmount = CurrMaxAmount + ((primeMaxAmount - CurrMaxAmount) * AgeTraitObj.AgeMultiplier);
                else CurrAmount = (defaultToMaxWhenDegrading) ? primeMaxAmount : startingMaxAmount;
            }
        }
    }

    /// <summary>
    /// An extension of a dynamic stat, this stat adds support for being affected by the creature's age.
    /// </summary>
    [Serializable]
    public class AgeRangeStat : RangeStat
    {
        #region Variables
        [Tooltip("Update the current amount when growing towards prime?")]
        [SerializeField] private bool updateWhenGrowing = true;
        [Tooltip("Default to max or min when 'updateWhenGrowing' is false?")]
        [SerializeField] private bool defaultToMaxWhenGrowing = true;
        [Tooltip("Update the current amount when growing past prime?")]
        [SerializeField] private bool updateWhenDegrading = true;
        [Tooltip("Default to max or min when 'updateWhenDegrading' is false?")]
        [SerializeField] private bool defaultToMaxWhenDegrading = true;

        /// <summary>
        /// The AgeTrait this stat should use to update its max with the current age.
        /// </summary>
        public AgeTrait AgeTraitObj { get; set; }
        #endregion

        public AgeRangeStat(float sa, float smaxa, bool uwma, float smina, bool updateWhenGrowing, bool defaultToMaxWhenGrowing, bool updateWhenDegrading, bool defaultToMaxWhenDegrading, AgeTrait ageTrait) : base(sa, smaxa, uwma, smina)
        {
            this.updateWhenGrowing = updateWhenGrowing;
            this.defaultToMaxWhenGrowing = defaultToMaxWhenGrowing;
            this.updateWhenDegrading = updateWhenDegrading;
            this.defaultToMaxWhenDegrading = defaultToMaxWhenDegrading;
            AgeTraitObj = ageTrait;
        }

        /// <summary>
        /// Update the current amount via the age multiplier from the age trait.
        /// </summary>
        public void UpdateAgeValue()
        {
            if (AgeTraitObj == null) return;

            if (AgeTraitObj.IsGrowing)
            {
                if (updateWhenGrowing) CurrAmount = AgeTraitObj.AgeMultiplier * (CurrMaxAmount - CurrMinAmount);
                else CurrAmount = (defaultToMaxWhenGrowing) ? CurrMaxAmount : CurrMinAmount;
            }
            else
            {
                if (updateWhenDegrading) CurrAmount = AgeTraitObj.AgeMultiplier * (CurrMaxAmount - CurrMinAmount);
                else CurrAmount = (defaultToMaxWhenDegrading) ? CurrMaxAmount : CurrMinAmount;
            }
        }
    }
}
