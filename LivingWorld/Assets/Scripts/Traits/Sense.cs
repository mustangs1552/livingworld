﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// A sense controlled by the sense trait.
    /// </summary>
    [RequireComponent(typeof(SensesTrait))]
    public abstract class Sense : MonoBehaviour
    {
        [Tooltip("Enable debugging?")]
        [SerializeField] protected bool isDebug = false;

        /// <summary>
        /// This sense's main Sense Trait.
        /// </summary>
        public SensesTrait MainSensesTrait
        {
            get
            {
                return sensesTrait;
            }
        }
        private SensesTrait sensesTrait = null;

        /// <summary>
        /// Checks to see what objects this sense detects.
        /// </summary>
        /// <returns>The objects detected by this sense.</returns>
        public abstract List<Transform> CheckSense();
        /// <summary>
        /// Checks to see if any of the given objects are detected by this sense.
        /// </summary>
        /// <returns>The objects detected by this sense.</returns>
        public abstract List<Transform> CheckSense(List<Transform> objs);

        /// <summary>
        /// Setup this sense.
        /// </summary>
        public abstract void SenseSetup();
        /// <summary>
        /// Reset this sense.
        /// </summary>
        public abstract void SenseReset();

        /// <summary>
        /// Initial setup.
        /// </summary>
        /// <param name="st">This sense's main Sense Trait.</param>
        public void Setup(SensesTrait st)
        {
            sensesTrait = st;
        }

        /// <summary>
        /// Find the objects that are eligible to start filtering for detectable objects by this sense.
        /// </summary>
        /// <returns></returns>
        protected abstract List<Transform> FindEligibleObjects();

        /// <summary>
        /// Runs a test that gets all sensed objects and shows the results via debug lines connecting those objects.
        /// Green: Did sense
        ///   Red: Did not sense
        /// </summary>
        private void SenseTest()
        {
            List<Transform> startingObjs = FindEligibleObjects();
            List<Transform> endingObjs = CheckSense(startingObjs);

            foreach (Transform obj in startingObjs) Debug.DrawLine(transform.position, obj.position, (endingObjs.Contains(obj)) ? Color.green : Color.red);
        }

        protected virtual void OnDrawGizmos()
        {
            if (isDebug) SenseTest();
        }
    }
}