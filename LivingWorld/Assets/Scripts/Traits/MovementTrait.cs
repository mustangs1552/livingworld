﻿using System;
using UnityEngine;
using UnityEngine.AI;
using MattRGeorge.Unity.AI.AIIndividual;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// This trait uses Unity's Nav Mesh to control the AI's movement through the world.
    /// </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    public class MovementTrait : PassiveTrait
    {
        #region Variables
        [Tooltip("Enable debugging?")]
        [SerializeField] private bool isDebug = false;
        [Tooltip("The movement speed stat of the AI.")]
        [SerializeField] private AgeRangeStat speedStat = null;
        [Tooltip("The multiplier to be applied to the movement speed stat while running.")]
        [SerializeField] private float runningSpeedMultiplier = 2;
        [Tooltip("The distance from the target for the AI to consider it reached.")]
        [SerializeField] private AgeRangeStat stopDistStat = null;
        [Tooltip("The speed of the extra rotation that is applied due to Unity's NavMeshAgent's rotation being slow and strange to control.")]
        [SerializeField] private float extraRotSpeed = 1;

        private NavMeshAgent navAgent = null;
        private Vector3 currTarget = Vector3.zero;
        private bool isRunning = false;
        private Action currTargetCallbacks = null;
        #endregion

        #region Methods
        public override void PreTraitSetup()
        {
            navAgent = GetComponent<NavMeshAgent>();
            speedStat.StatReset();
            stopDistStat.StatReset();
        }
        public override void TraitSetup()
        {

        }
        public override void PostTraitSetup()
        {

        }

        public override void UpdateTrait()
        {
            if (CheckTargetReached())
            {
                CancelTarget();
                currTargetCallbacks?.Invoke();
            }
            else
            {
                UpdateSpeed();
                UpdateRotation();
            }
        }

        public override void TraitReset()
        {
            CancelTarget();
            speedStat.StatReset();
            stopDistStat.StatReset();
        }

        /// <summary>
        /// Set the destination for the NavMeshAgent.
        /// </summary>
        /// <param name="targetPos">The destination.</param>
        /// <param name="run">Run to the target?</param>
        /// <param name="targetReachedCallbacks">Callbacks to be called when target is reached.</param>
        public void AssignTarget(Vector3 targetPos, bool run = false, Action targetReachedCallbacks = null)
        {
            if (navAgent != null && navAgent.enabled)
            {
                navAgent.SetDestination(targetPos);
                currTarget = targetPos;
                isRunning = run;
                currTargetCallbacks = targetReachedCallbacks;
            }
        }

        /// <summary>
        /// Cancel the path on the NavMeshAgent.
        /// </summary>
        public void CancelTarget()
        {
            if (navAgent != null)
            {
                navAgent.ResetPath();
                currTarget = Vector3.zero;
                currTargetCallbacks = null;
            }
        }

        /// <summary>
        /// Check if destination was reached via distance.
        /// </summary>
        /// <returns>True if reached.</returns>
        public bool CheckTargetReached()
        {
            if (currTarget == null || currTarget == Vector3.zero) return true;

            stopDistStat.UpdateAgeValue();

            if (CheckTargetDist() <= stopDistStat.CurrAmount) return true;
            else return false;
        }
        /// <summary>
        /// Check the distance from the destination.
        /// </summary>
        /// <returns>The distance.</returns>
        public float CheckTargetDist()
        {
            if (navAgent != null)
            {
                float dist = Vector3.Distance(transform.position, navAgent.destination) - transform.localScale.z / 2;
                return (dist >= 0) ? dist : 0;
            }

            return -1;
        }

        /// <summary>
        /// Update the speed stats value and apply it to the nav mesh agent.
        /// </summary>
        private void UpdateSpeed()
        {
            if (currTarget == null || currTarget == Vector3.zero) return;

            speedStat.UpdateAgeValue();
            if (!isRunning) navAgent.speed = speedStat.CurrAmount;
            else navAgent.speed = speedStat.CurrAmount * runningSpeedMultiplier;
        }

        /// <summary>
        /// Add an extra rotation because of Unity's nav mesh agent having a strange way to handle rotations and were very slow.
        /// </summary>
        private void UpdateRotation()
        {
            Vector3 lookDir = navAgent.steeringTarget - transform.position;
            if (lookDir != Vector3.zero)
            {
                Quaternion lookRot = Quaternion.LookRotation(lookDir);
                Quaternion lookRotY = Quaternion.Euler(transform.rotation.eulerAngles.x, lookRot.eulerAngles.y, transform.rotation.eulerAngles.z);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotY, extraRotSpeed * Time.deltaTime);
            }
        }
        #endregion

        private void OnDrawGizmos()
        {
            if (isDebug && currTarget != Vector3.zero)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, currTarget);
            }
        }
    }
}
