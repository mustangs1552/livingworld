﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.ObjectPooling;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// This sense is the AI's ability to see other objects.
    /// It uses a FOV and range to check what is sensed.
    /// </summary>
    public class SightSense : Sense
    {
        #region Variables
        [Tooltip("The distance this sense can detect an object.")]
        [SerializeField] private AgeRangeStat rangeStat = null;
        [Tooltip("The horizontal FOV of this creature.")]
        [SerializeField] private float horizontalFOV = 180;
        [Tooltip("The vertical FOV of this creature.")]
        [SerializeField] private float verticalFOV = 90;

        private float verticalFOVCenter = 0;
        #endregion

        #region Methods
        #region Public
        public override List<Transform> CheckSense()
        {
            return CheckSight(FindEligibleObjects());
        }
        public override List<Transform> CheckSense(List<Transform> objs)
        {
            return CheckSight(objs);
        }

        public override void SenseSetup()
        {
            Setup();
        }

        public override void SenseReset()
        {

        }
        #endregion

        #region private
        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            rangeStat.AgeTraitObj = MainSensesTrait.Brain.GetTrait<AgeTrait>();
            rangeStat.StatReset();

            verticalFOVCenter = Vector3.Angle(Vector3.forward, Vector3.up);
        }

        protected override List<Transform> FindEligibleObjects()
        {
            List<Transform> objManagerObjs = new List<Transform>();

            if (ObjectPoolManager.SINGLETON != null)
            {
                objManagerObjs = ObjectPoolManager.SINGLETON.AllTransforms;
                objManagerObjs.Remove(gameObject.transform);
            }

            return objManagerObjs;
        }

        /// <summary>
        /// Check the list of valid objects to see which are actually sensed.
        /// </summary>
        /// <param name="validObjs">The list of valid objects.</param>
        /// <returns>The list of sensed objects.</returns>
        private List<Transform> CheckSight(List<Transform> validObjs)
        {
            List<Transform> detectedObjs = new List<Transform>();

            rangeStat.UpdateAgeValue();
            foreach (Transform obj in validObjs)
            {
                // Within range and FOV
                float objDist = Vector3.Distance(transform.position, obj.position);
                if (objDist <= rangeStat.CurrAmount && CheckFOV(obj))
                {
                    // LOS
                    RaycastHit hit = new RaycastHit();
                    if (Physics.Raycast(new Ray(transform.position, obj.position - transform.position), out hit, objDist))
                    {
                        if (hit.transform == obj) detectedObjs.Add(obj);
                    }
                }
            }

            return detectedObjs;
        }
        /// <summary>
        /// Check to see if the given object is within the FOV range.
        /// </summary>
        /// <param name="obj">The object to check.</param>
        /// <returns>True if within FOV range.</returns>
        private bool CheckFOV(Transform obj)
        {
            Vector3 targetDir = obj.position - transform.position;
            float horizontalAngle = Vector3.Angle(targetDir, transform.forward);
            float verticalAngle = Vector3.Angle(targetDir, transform.up);

            if (horizontalAngle <= horizontalFOV / 2)
            {
                if (verticalAngle <= verticalFOVCenter + verticalFOV / 2 && verticalAngle >= verticalFOVCenter - verticalFOV / 2) return true;
            }

            return false;
        }
        #endregion
        #endregion

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();

            if (isDebug)
            {
                rangeStat.UpdateAgeValue();
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, rangeStat.CurrAmount);
            }
        }
    }
}