﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.ObjectPooling;
using MattRGeorge.Unity.AI.AIIndividual;

namespace Assets.Scripts.Traits
{
    public class GenderReproductionTrait : AsexualReproductionTrait
    {
        private enum ReproStates
        {
            Moving,
            Mating,
            Pregnant,
            GivingBirth,
            None
        }

        #region Variables
        [Tooltip("The gender of this species that gives birth.")]
        [SerializeField] private Genders pregnantGender = Genders.Female;
        [Tooltip("The duration i8n seconds of the mating process.")]
        [SerializeField] private float matingDurationSecs = 3;
        [Tooltip("The duration in days of the pregnancy period.")]
        [SerializeField] private int pregnancyDurationDays = 30;

        /// <summary>
        /// The current gender of this creature.
        /// </summary>
        public Genders Gender
        {
            get
            {
                return gender;
            }
        }

        private Genders gender = Genders.Male;
        private GenderReproductionTrait currMate = null;
        private SensesTrait sensesTrait = null;
        private MovementTrait movementTrait = null;
        private ReproStates state = ReproStates.None;
        private float matingStarted = -1;
        private float pregnancyStarted = -1;
        #endregion

        #region Methods
        #region AITrait Methods
        public override bool Checking()
        {
            return base.Checking() || currMate != null;
        }
        public override bool Performing()
        {
            if (currMate == null)
            {
                currMate = FindMate();
                if (currMate != null)
                {
                    bool mateAccepted = currMate.RequestMate(this);
                    if (!mateAccepted) currMate = null;
                    else state = ReproStates.Moving;
                }
            }
            else
            {
                switch (state)
                {
                    case ReproStates.Moving:
                        movementTrait.AssignTarget(currMate.transform.position, false, MateReached);
                        break;
                    case ReproStates.Mating:
                        if (Time.time - matingStarted >= matingDurationSecs)
                        {
                            state = ReproStates.Pregnant;
                            matingStarted = -1;
                        }
                        break;
                    case ReproStates.Pregnant:
                        if (Gender != pregnantGender)
                        {
                            lastBirthDays = ageTrait.Age.totalDays;
                            currMate = null;
                            return false;
                        }
                        else if (pregnancyStarted < 0) pregnancyStarted = ageTrait.Age.totalDays;
                        else if (ageTrait.Age.totalDays - pregnancyStarted >= pregnancyDurationDays)
                        {
                            state = ReproStates.GivingBirth;
                            pregnancyStarted = -1;
                        }
                        break;
                    case ReproStates.GivingBirth:
                        if (!base.Performing()) // Base Performing() handles giving birth.
                        {
                            state = ReproStates.None;
                            currMate = null;
                            return false;
                        }
                        break;
                }
            }

            return true;
        }

        public override void TraitSetup()
        {
            base.TraitSetup();

            sensesTrait = Brain.GetTrait<SensesTrait>();
            movementTrait = Brain.GetTrait<MovementTrait>();
        }
        public override void PostTraitSetup()
        {
            base.PostTraitSetup();

            ChooseGender();
        }

        public override void TraitReset()
        {
            base.TraitReset();

            currMate = null;
            state = ReproStates.None;
            matingStarted = -1;
            pregnancyStarted = -1;
            
            ChooseGender();
        }
        #endregion

        public bool IsMateAvailable(GenderReproductionTrait requestingMate)
        {
            if (currMate != null && currMate != requestingMate) return false;
            else if (currMate == null) return true;

            return false;
        }
        public bool RequestMate(GenderReproductionTrait requestingMate)
        {
            if (IsMateAvailable(requestingMate))
            {
                currMate = requestingMate;
                return true;
            }

            return false;
        }

        #region Private
        private void MateReached()
        {
            state = ReproStates.Mating;
            matingStarted = Time.time;
        }

        private void ChooseGender()
        {
            int randNum = Random.Range(0, 2);
            gender = (randNum == 0) ? Genders.Male : Genders.Female;

            Debug.Log(gender.ToString());
        }

        private bool CheckIfValidMate(GenderReproductionTrait mate = null)
        {
            if (mate == null && currMate == null) return false;
            else if (mate == null) mate = currMate;

            return mate.Gender != gender && mate.CanGiveBirth && mate.IsMateAvailable(this);
        }
        private List<GenderReproductionTrait> FilterValidMates(List<GameObject> sameSpecies)
        {
            sameSpecies.Remove(gameObject);

            List<GenderReproductionTrait> validMates = new List<GenderReproductionTrait>();
            foreach (GameObject other in sameSpecies)
            {
                AIBrain otherBrain = other.GetComponent<AIBrain>();
                if (otherBrain != null)
                {
                    GenderReproductionTrait otherReproTrait = otherBrain.GetTrait<GenderReproductionTrait>();
                    if (otherReproTrait != null)
                    {
                        if (CheckIfValidMate(otherReproTrait)) validMates.Add(otherReproTrait);
                    }
                }
            }

            return validMates;
        }
        private GenderReproductionTrait FindMate()
        {
            List<GenderReproductionTrait> validMates = FilterValidMates(ObjectPoolManager.SINGLETON.GetActiveObjects(gameObject));
            List<Transform> trans = new List<Transform>();
            validMates.ForEach(x => trans.Add(x.transform));
            trans = sensesTrait.CheckSenses(trans);
            Transform closest = TransformUtility.GetClosest(transform.position, trans);

            if (closest != null)
            {
                AIBrain closestBrain = closest.GetComponent<AIBrain>();
                if (closestBrain != null)
                {
                    GenderReproductionTrait closestReproTrait = closestBrain.GetTrait<GenderReproductionTrait>();
                    if (closestReproTrait != null) return closestReproTrait;
                }
            }

            return null;
        }
        #endregion
        #endregion
    }

    public enum Genders
    {
        Male,
        Female
    }
}
