﻿using UnityEngine;
using MattRGeorge.Unity.AI.AIIndividual;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// This trait handles the size of the creature via its age.
    /// </summary>
    public class GrowthTrait : PassiveTrait
    {
        #region Variables
        [Tooltip("The size stat to use.")]
        [SerializeField] private AgeRangeStat sizeStat = null;
        [Tooltip("The frequency that the stat updates its value.")]
        [SerializeField] private float updateFreq = 1;

        private AgeTrait ageTrait = null;
        private float lastUpdate = 0;
        #endregion

        #region Methods
        public override void UpdateTrait()
        {
            UpdateScale();
        }

        public override void PreTraitSetup()
        {

        }
        public override void TraitSetup()
        {
            ageTrait = (AgeTrait)Brain.GetTrait(typeof(AgeTrait));

            sizeStat.AgeTraitObj = ageTrait;
            sizeStat.StatReset();

            lastUpdate = Time.time - updateFreq;
        }
        public override void PostTraitSetup()
        {
            
        }

        public override void TraitReset()
        {
            sizeStat.StatReset();
            lastUpdate = Time.time - updateFreq;
        }

        /// <summary>
        /// Update the stat and the scale of the creature.
        /// </summary>
        private void UpdateScale()
        {
            if (Time.time - lastUpdate >= updateFreq)
            {
                sizeStat.UpdateAgeValue();
                transform.localScale = new Vector3(sizeStat.CurrAmount, sizeStat.CurrAmount, sizeStat.CurrAmount);

                lastUpdate = Time.time;
            }
        }
        #endregion
    }
}
