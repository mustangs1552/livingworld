﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.ObjectPooling;

namespace Assets.Scripts.Traits
{
    /// <summary>
    /// This sense is the creature's ability to hear other objects.
    /// This looks for Audio Source objects which represents a sound.
    /// </summary>
    public class HearingSense : Sense
    {
        [Tooltip("The range that this creature can hear an audio source's range.")]
        [SerializeField] private AgeRangeStat rangeStat = null;
        [Tooltip("The accuracy of this creature's ability to detect an audio source.")]
        [SerializeField] private AgeRangeStat accuracyStat = null;

        #region Methods
        public override List<Transform> CheckSense()
        {
            return CheckHearing(FindEligibleObjects());
        }
        public override List<Transform> CheckSense(List<Transform> objs)
        {
            return CheckHearing(objs);
        }

        public override void SenseReset()
        {
            rangeStat.StatReset();
            accuracyStat.StatReset();
        }
        public override void SenseSetup()
        {
            AgeTrait ageTrait = MainSensesTrait.Brain.GetTrait<AgeTrait>();
            if (ageTrait != null)
            {
                rangeStat.AgeTraitObj = ageTrait;
                accuracyStat.AgeTraitObj = ageTrait;
            }
            SenseReset();
        }

        /// <summary>
        /// Calculate for the accuracy for sensing the given audio source.
        /// </summary>
        /// <param name="audioSource">The audio source to check the accuracy of.</param>
        /// <returns>A value between 0 and 1 representing the accuracy.</returns>
        private float CalcAccuracy(AudioSource audioSource)
        {
            if (audioSource == null) return 0;

            rangeStat.UpdateAgeValue();
            accuracyStat.UpdateAgeValue();

            float dist = Vector3.Distance(transform.position, audioSource.transform.position);
            float maxDist = rangeStat.CurrAmount + audioSource.maxDistance;
            if (dist >= maxDist) return 0;
            else return (((maxDist - dist) / maxDist) + accuracyStat.CurrAmountPerc) / 2;
        }
        /// <summary>
        /// Check the given list of audio sources to see if this creature can hear any of them.
        /// </summary>
        /// <param name="audioSources">The audio sources to check.</param>
        /// <returns>The audio sources' transfoms that this creature hears.</returns>
        private List<Transform> CheckHearing(List<Transform> audioSources)
        {
            if (audioSources == null || audioSources.Count == 0) return new List<Transform>();

            List<Transform> sensedScents = new List<Transform>();
            foreach (Transform audioSourcesTrans in audioSources)
            {
                AudioSource audioSource = audioSourcesTrans.GetComponent<AudioSource>();
                if (audioSource != null)
                {
                    float accuracy = CalcAccuracy(audioSource);
                    if (accuracy <= 0) continue;
                    else if (accuracy >= 1) sensedScents.Add(audioSourcesTrans);
                    else
                    {
                        int randNum = Random.Range(0, 100);
                        if (accuracy >= randNum / 100) sensedScents.Add(audioSourcesTrans);
                    }
                }
            }

            return sensedScents;
        }

        protected override List<Transform> FindEligibleObjects()
        {
            List<AudioSource> audioSources = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AudioSource>();
            List<Transform> audioSourceTrans = new List<Transform>();
            audioSources.ForEach(x => audioSourceTrans.Add(x.transform));
            return audioSourceTrans;
        }
        #endregion

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();

            if (isDebug)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, rangeStat.CurrAmount);
            }
        }
    }
}
