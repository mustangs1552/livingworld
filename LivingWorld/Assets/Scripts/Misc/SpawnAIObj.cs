﻿using UnityEngine;
using MattRGeorge.Unity.Misc;
using Assets.Scripts.Traits;

namespace Assets.Scripts.Misc
{
    /// <summary>
    /// This version of the SpawnObject spawns an AI object and perform certain actions specific for an AI.
    /// </summary>
    public class SpawnAIObj : SpawnObject
    {
        /// <summary>
        /// Called after a time after the object is spawned.
        /// </summary>
        private void PostSpawn()
        {
            if (SpawnedObj.GetComponent<AgeTrait>() != null) SpawnedObj.GetComponent<AgeTrait>().LateBorn(100);
        }

        private new void Awake()
        {
            base.Awake();

            if (Obj.GetComponent<AgeTrait>() == null) Debug.LogError("'obj' must be an AI with an 'AgeTrait'!");
            if (Obj.GetComponent<AgeTrait>() == null) Debug.LogError("'obj' must be an AI with a 'MovementTrait'!");
            OnPostSpawn += PostSpawn;
        }
    }
}
