﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.ObjectPooling;
using MattRGeorge.Unity.AI.AIIndividual;
using Assets.Scripts.Traits;

namespace Assets.Scripts.Misc
{
    public class AIMovementDriver : MonoBehaviour
    {
        [SerializeField] private Transform targetPos = null;

        private void Update()
        {
            if(targetPos != null)
            {
                List<AIBrain> aiBrains = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AIBrain>();
                foreach(AIBrain brain in aiBrains)
                {
                    MovementTrait movement = brain.GetTrait<MovementTrait>();
                    if (movement != null) movement.AssignTarget(targetPos.position);
                }
            }
        }
    }
}
