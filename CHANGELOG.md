# [Changelog]

Uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html) for versioning and [keep a chaneglog](https://keepachangelog.com/en/1.0.0/) for changelog's format.

## [Unreleased]
### Added
- Stats
- Age Trait
- Day Night Cycle
- Growth Trait
- Movement Trait

## [0.0.0] - 12/6/2019
### Added
- Unity project
- Changelog
- MattRGeorge package